# Alfred GitLab

Alfred Workflow for Navigating GitLab

This workflow includes shortcuts to navigate to commonly used workflow on GitLab.

## Usage

Available Alfred keywords:
- `gl`: List all available workflows
- `gli`: Search or create issues
- `glm`: Search merge requests
- `glo`: Open group or project page
- `glc`: Alfred GitLab configuration to add/delete group and project

To start, add a few commonly used projects and groups to Alfred GitLab
with `glc`.
1. `glc`
1. Select "Add GitLab Project"
1. Enter the project path (e.g `gitlab-org/gitlab`)

To search issues in `gitlab-org/gitlab`:
1. `gl` or `gli`
1. Select "Search Issues"
1. Select from one of the preconfigured projects or groups (e.g `gitlab-org/gitlab`)
1. Enter the search terms

## Features

Currently supported workflows are:
- [x] Search issues
- [x] Search MRs
- [x] Create new issue
- [x] Open group or project page

More to be added:
- [ ] Open project pipelines
- [ ] Search gitlab documentation
- [ ] Search gitlab handbook

This workflow stores its configuration in `~/.alfred-gitlab.json`.

This workflow uses Mac OS X's system ruby (/usr/bin/ruby) to run ruby scripts.
Tested on ruby 2.3.7.